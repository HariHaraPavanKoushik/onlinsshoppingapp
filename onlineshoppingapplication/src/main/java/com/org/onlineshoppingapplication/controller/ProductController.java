package com.org.onlineshoppingapplication.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.org.onlineshoppingapplication.dto.OrderDto;
import com.org.onlineshoppingapplication.dto.ProductDto;
import com.org.onlineshoppingapplication.dto.ProductResponse;
import com.org.onlineshoppingapplication.dto.ResponseDto;
import com.org.onlineshoppingapplication.entity.Product;
import com.org.onlineshoppingapplication.exception.CustomException;
import com.org.onlineshoppingapplication.exception.ProductDetailsException;
import com.org.onlineshoppingapplication.exception.ProductException;
import com.org.onlineshoppingapplication.exception.QuantityNotAvailable;
import com.org.onlineshoppingapplication.exception.RegisterException;
import com.org.onlineshoppingapplication.exception.UserNotLoginException;
import com.org.onlineshoppingapplication.service.ProductService;

/**
 * 
 * @author Koushik
 * @since 23-03-2020
 *
 */
@RestController
@CrossOrigin(origins ="*", allowedHeaders= "*")
public class ProductController {

	@Autowired
	ProductService productService;

	/**
	 * In this functionality we can fetch the list of products based on the type of
	 * user
	 * 
	 * @param emailId
	 * @throws UserNotLoginException
	 * @throws CustomException
	 * @return productDto -List of products
	 * 
	 */
	@GetMapping("/products")
	public ResponseEntity<List<Product>> products(@RequestParam("emailId") String emailId)
			throws UserNotLoginException, CustomException {
		List<Product> product = productService.products(emailId);
		return new ResponseEntity<>(product, HttpStatus.OK);
	}

	/**
	 * In this functionality user will be able to buy a product
	 * 
	 * @RequestBody orderDto
	 * @return
	 * @throws CustomException
	 * @throws RegisterException
	 * @throws QuantityNotAvailable
	 * 
	 */
	@PostMapping("/orders")
	public ResponseEntity<ResponseDto> buyProduct(@Valid @RequestBody OrderDto orderDto)
			throws CustomException, RegisterException, QuantityNotAvailable {
		ResponseDto responseDto = productService.buyProduct(orderDto);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);

	}

	/**
	 * In this Functionality we are fetching the products by productName
	 * 
	 * @param productName
	 * @return
	 * @throws CustomException
	 */
	@GetMapping("/product")
	public ResponseEntity<List<Product>> productsbyName(@RequestParam("productName") String productName)
			throws CustomException {
		List<Product> product = productService.getProductsByName(productName);
		return new ResponseEntity<>(product, HttpStatus.OK);
	}

	/**
	 * @author Uma
	 * @since 26-03-2020
	 * @param ProductDto-contains the request params productname,
	 *                            productcost,availability and producttype
	 * @ ProductResponse- contains success or failure message as a response
	 * @return ResponseEntity Object along with status code and message
	 * @throws ProductDetailsException
	 * @throws ProductException
	 */
	@PostMapping("/admin/products")
	public ResponseEntity<ProductResponse> addProductByAdmin(@RequestBody ProductDto productDto)
			throws ProductDetailsException, ProductException {
		ProductResponse productResponse = productService.addProductByAdmin(productDto);
		return new ResponseEntity<>(productResponse, HttpStatus.OK);

	}
}
