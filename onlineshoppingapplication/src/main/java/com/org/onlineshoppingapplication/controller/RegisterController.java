package com.org.onlineshoppingapplication.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.onlineshoppingapplication.dto.RegisterResponse;
import com.org.onlineshoppingapplication.dto.UserDto;
import com.org.onlineshoppingapplication.service.RegisterService;

/**
 * 
 * @author Ashan
 * @since 23-03-2020
 *
 */
@RestController
@RequestMapping("/users")
@CrossOrigin(origins ="*", allowedHeaders= "*")
public class RegisterController {

	@Autowired
	RegisterService registerService;
	
	/**
	 * This functionality is used for registering the User
	 * 
	 * @param userDto
	 * @return 	-save user data and gives status code and success message
	 */
	@PostMapping("")
	public ResponseEntity<RegisterResponse> register(@Valid @RequestBody UserDto userDto) {
		return new ResponseEntity<>(registerService.register(userDto), HttpStatus.CREATED);
	}

}
