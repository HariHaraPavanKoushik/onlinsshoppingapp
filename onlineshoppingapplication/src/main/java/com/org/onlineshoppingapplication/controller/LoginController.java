package com.org.onlineshoppingapplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.org.onlineshoppingapplication.dto.LoginDto;
import com.org.onlineshoppingapplication.dto.LoginResponseDto;
import com.org.onlineshoppingapplication.service.LoginService;

/**
 * 
 * @author Uma
 * @since 2020-03-23
 *
 */
@RestController
@CrossOrigin(origins ="*", allowedHeaders= "*")

public class LoginController {
	@Autowired
	LoginService loginService;

	/**
	 * This method is used to validate the user login
	 * 
	 
	 * @param LoginDto	-contains the request params EmailId and password ResponseDto-
	 *                          contains success or failure message as a response
	 * @return ResponseEntity Object along with status code and message
	 */

	@PostMapping("/login")
	public ResponseEntity<LoginResponseDto> userLogin(@RequestBody LoginDto loginDto) {
		LoginResponseDto responseDto = loginService.userLogin(loginDto);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

}