package com.org.onlineshoppingapplication.controller;

 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.org.onlineshoppingapplication.dto.OrderResponse;
import com.org.onlineshoppingapplication.dto.OrdersDto;
import com.org.onlineshoppingapplication.service.OrderService;

 

@RestController
@CrossOrigin(origins ="*", allowedHeaders= "*")
public class OrderController {
    
    @Autowired
    OrderService orderService;
    
    
    @PutMapping("/users/{userId}/products/{productId}/rating")
    public ResponseEntity<OrderResponse> userRatings(@PathVariable Long userId,@PathVariable Long productId,@RequestBody OrdersDto orderDto) {
        
        OrderResponse orderResponse=orderService.rating(productId,userId,orderDto);
        return new ResponseEntity<>(orderResponse, HttpStatus.OK);
        
    }
    
    

 

}