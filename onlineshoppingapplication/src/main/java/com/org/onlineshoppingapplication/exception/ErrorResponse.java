package com.org.onlineshoppingapplication.exception;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ErrorResponse{

	private String message;
	private int status;

	public ErrorResponse() {
		super();
	}

	
}
