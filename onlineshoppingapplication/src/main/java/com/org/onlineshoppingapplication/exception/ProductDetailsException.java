
package com.org.onlineshoppingapplication.exception;

public class ProductDetailsException extends Exception {

	private static final long serialVersionUID = 1L;

	public ProductDetailsException(String message) {
		super(message);
	}

	public ProductDetailsException(int pleaseproviderequireddata) {

	}
}
