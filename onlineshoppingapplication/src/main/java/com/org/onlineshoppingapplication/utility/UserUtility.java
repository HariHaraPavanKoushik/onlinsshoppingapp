package com.org.onlineshoppingapplication.utility;

public class UserUtility {

	public static final String USER_NOT_LOGIN = "User did not Logged In";
	public static final int USER_NOT_LOGIN_STATUS = 605;

	public static final String USER_NOT_EXIST = "User is not THERE";
	public static final int USER_NOT_EXIST_STATUS = 606;

	public static final String SUCCESSSFULLY_SAVED_DATA = "User registered Successfully!";
	public static final int SUCCESSSFULLY_SAVED_DATA_STATUS = 607;
	
	public static final String USER_NOT_FOUND = "User Already Exist! ";
	public static final int USER_NOT_FOUND_STATUS = 608;

	public static final String USER_WELCOME = "You have successfully logged in";
	public static final int USER_WELCOME_STATUS = 609;
	
	public static final String USER_ERROR_STATUS = "User not found";
	public static final int USER_STATUSCODE_ERROR = 610;
	
	public static final String BOUGHT_SUCCESSSFULLY = "User Bought Successfully!";
	public static final int BOUGHT_SUCCESSSFUL_STATUS = 612;
	
	public static final String PRODUCT_NOT_EXIST = "Product is not THERE";
	public static final int PRODUCT_NOT_EXIST_STATUS = 606;
	
	public static final String QUANTITY_NOT_AVAILABLE = "Out of Quantity";
	public static final int QUANTITY_NOT_AVAILABLE_STATUS = 615;
	
	public static final String NO_PRODUCT_PRESENT = "No Product for this User";
    public static final int NO_PRODUCT_PRESENT_ERRORCODE = 777;
    
    public static final String RATTED_SUCCESSFULLY = "RATING SUBMITTED!";
    public static final int RATTED_SUCCESSFUL_CODE=800;
    
    public static final String NO_PRODUCT_FOUND = "No Product Found with the Product Name";
    public static final int NO_PRODUCT_FOUND_STATUS = 700;
    
    public static final String Try_AGAIN = "please provide required data";
    public static final int Try_AGAIN_STATUS = 611;
    
    public static final String PRODUCTS_LIST_ERROR = "Already same product data exists";
    public static final int PRODUCTS_LIST_ERROR_STATUS = 700;

    public static final String SUCCESSSFULLY_SAVED_PRODUCT_DATA = "Product saved Successfully!";
    public static final int SUCCESSSFULLY_SAVED_PRODUCT_DATA_STATUS = 701;
    
	public static final String ORDER_NOT_EXIST = "Order does not Exist";
	public static final int ORDER_NOT_EXIST_STATUS = 705;

	private UserUtility() {

	}

}
