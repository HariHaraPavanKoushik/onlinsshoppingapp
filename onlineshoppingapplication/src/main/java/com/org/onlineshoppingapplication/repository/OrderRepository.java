package com.org.onlineshoppingapplication.repository;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.org.onlineshoppingapplication.entity.Product;
import com.org.onlineshoppingapplication.entity.UserOrder;

@Repository
@Transactional
public interface OrderRepository extends JpaRepository<UserOrder, Long>{

	/*
	 * @Modifying
	 * 
	 * @Query("UPDATE UserOrder entity SET entity.productRating =?3 WHERE entity.productId=?1 AND entity.userId=?2"
	 * ) void updateRating(long productId, long userId, int productRating);
	 */
	
	UserOrder findByUserRegisterIdAndProductProductId(long registerId, long productId);

	Optional<UserOrder> findByUserRegisterId(Long userId);

	Optional<Product> findByProductProductId(Long productId);

	

}
