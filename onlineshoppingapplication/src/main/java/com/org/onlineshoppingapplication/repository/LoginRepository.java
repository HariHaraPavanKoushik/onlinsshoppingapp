package com.org.onlineshoppingapplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.org.onlineshoppingapplication.entity.User;

/**
 * @author Uma
 *
 */
public interface LoginRepository extends JpaRepository<User, Long> {

	User findByEmailIdAndPassword(String emailId, String password);

}
