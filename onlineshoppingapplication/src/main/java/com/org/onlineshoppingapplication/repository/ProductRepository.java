package com.org.onlineshoppingapplication.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.org.onlineshoppingapplication.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{

	@Query("select entity from Product entity where entity.productType=?1")
	public List<Product> getProductDetails(String productType);
	
	public Product findByProductId(long productId);
	
	@Query("Select entity from Product entity where entity.productName LIKE concat('%',:productName,'%')")
    public List<Product> fetchProductByName(String productName);
	
	@Query("Select entity from Product entity where entity.productName=?1 AND entity.productType =?2 AND entity.productCost =?3")
    public Product findByProductNameAndTypeAndCost(String productName, String productType, double productCost);

	
}
