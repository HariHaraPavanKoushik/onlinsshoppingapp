package com.org.onlineshoppingapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineshoppingapplicationApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineshoppingapplicationApplication.class, args);
	}

}
