package com.org.onlineshoppingapplication.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.org.onlineshoppingapplication.dto.OrderDto;
import com.org.onlineshoppingapplication.dto.ProductDto;
import com.org.onlineshoppingapplication.dto.ProductResponse;
import com.org.onlineshoppingapplication.dto.ResponseDto;
import com.org.onlineshoppingapplication.entity.Product;
import com.org.onlineshoppingapplication.entity.User;
import com.org.onlineshoppingapplication.entity.UserOrder;
import com.org.onlineshoppingapplication.exception.CustomException;
import com.org.onlineshoppingapplication.exception.ProductDetailsException;
import com.org.onlineshoppingapplication.exception.ProductError;
import com.org.onlineshoppingapplication.exception.ProductException;
import com.org.onlineshoppingapplication.exception.QuantityNotAvailable;
import com.org.onlineshoppingapplication.exception.RegisterException;
import com.org.onlineshoppingapplication.exception.UserNotLoginException;
import com.org.onlineshoppingapplication.repository.OrderRepository;
import com.org.onlineshoppingapplication.repository.ProductRepository;
import com.org.onlineshoppingapplication.repository.UserRepository;
import com.org.onlineshoppingapplication.utility.UserUtility;

/**
 * 
 * @author Koushik
 * @since 23-03-2020
 *
 */

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	OrderRepository orderRepository;

	/**
	 * In this Service Functionality we are passing emailId and fetching the
	 * userType and with that type we are fetching the product details
	 * 
	 * @param emailId
	 * @throws CustomException
	 * @throws UserNotLoginException
	 * @return -List of products based on userType
	 * 
	 */
	@Override
	public List<Product> products(String emailId) throws CustomException, UserNotLoginException {

		String userType = userRepository.getProductType(emailId);
		if (userType != null) {
			List<Product> product;
			if (userType.equalsIgnoreCase("low")) {
				product = productRepository.getProductDetails(userType);
			} else {
				product = productRepository.findAll();
			}
			return product;
		} else {
			throw new CustomException(UserUtility.USER_NOT_LOGIN);
		}

	}

	/**
	 * In this functionality user will be able to buy a product
	 * 
	 * @RequestBody orderDto
	 * @return
	 * @throws CustomException
	 * @throws RegisterException
	 * @throws QuantityNotAvailable 
	 * 
	 */
	@Override
	public ResponseDto buyProduct(OrderDto orderDto) throws CustomException, RegisterException, QuantityNotAvailable {
		User user = new User();
		long userid = orderDto.getUserId();
		long productid = orderDto.getProductId();
		Product product = new Product();
		UserOrder userOrder = new UserOrder();
		user = userRepository.findByRegisterId(userid);
		if (user == null) {
			throw new CustomException(UserUtility.USER_NOT_EXIST);
		}
		product = productRepository.findByProductId(productid);
		if (product == null) {
			throw new RegisterException(UserUtility.PRODUCT_NOT_EXIST);
		}
		if(product.getQuantity() < orderDto.getQuantity()) {
			throw new QuantityNotAvailable(UserUtility.QUANTITY_NOT_AVAILABLE);
		}
		BeanUtils.copyProperties(orderDto, userOrder);
		userOrder.setQuantity(userOrder.getQuantity());
		product.setQuantity(product.getQuantity()-userOrder.getQuantity());
		double cost = product.getProductCost();
		userOrder.setTotalcost(cost * userOrder.getQuantity());
		userOrder.setUser(user);
		userOrder.setProduct(product);
		UserOrder check = orderRepository.findByUserRegisterIdAndProductProductId(orderDto.getUserId(), orderDto.getProductId());
		if(check != null) {
			System.out.println(check.getQuantity());
			check.setQuantity(userOrder.getQuantity()+check.getQuantity());
			System.out.println(check.getQuantity());
 		check.setTotalcost(userOrder.getTotalcost()+check.getTotalcost());
 		orderRepository.save(check);
		}else {
		orderRepository.save(userOrder);
		}
		ResponseDto responseDto = new ResponseDto();
		responseDto.setStatusMessage("User bought the product Successfully");
		responseDto.setStatusCode(UserUtility.SUCCESSSFULLY_SAVED_DATA_STATUS);
		
		return responseDto;
	}
	
	@Override
    public List<Product> getProductsByName(String productName) throws ProductError {
        List<Product> list = new ArrayList<>();
        list = productRepository.fetchProductByName(productName);
        if(list == null || list.isEmpty()) {
            throw new ProductError(UserUtility.NO_PRODUCT_FOUND,UserUtility.NO_PRODUCT_FOUND_STATUS);
        }else {
            return list;
        }
    }
	
	/**
     * @author Uma
     * @since 26-03-2020
     * @param ProductDto-contains the request params productname,
     *                            productcost,availability and producttype
     * @ ProductResponse- contains success or failure message as a response
     * @return ResponseEntity Object along with status code and message
     * @throws ProductDetailsException
     * @throws ProductException
     */

    @Override
    public ProductResponse addProductByAdmin(ProductDto productDto) throws ProductDetailsException, ProductException {

        if (productDto == null)
            throw new ProductDetailsException(UserUtility.Try_AGAIN);

        String productName = productDto.getProductName();
        String productType = productDto.getProductType();
        double productCost = productDto.getProductCost();

        Product productIn = productRepository.findByProductNameAndTypeAndCost(productName, productType, productCost);
        Product product = new Product();
        BeanUtils.copyProperties(productDto, product);

        if (ObjectUtils.isEmpty(productIn)) {
            productRepository.save(product);
            return new ProductResponse(UserUtility.SUCCESSSFULLY_SAVED_PRODUCT_DATA, HttpStatus.CREATED.value());

        } else {
            return new ProductResponse(UserUtility.PRODUCTS_LIST_ERROR, HttpStatus.BAD_REQUEST.value());

        }
    }
}

