package com.org.onlineshoppingapplication.service;

import java.util.List;

import com.org.onlineshoppingapplication.dto.OrderDto;
import com.org.onlineshoppingapplication.dto.ProductDto;
import com.org.onlineshoppingapplication.dto.ProductResponse;
import com.org.onlineshoppingapplication.dto.ResponseDto;
import com.org.onlineshoppingapplication.entity.Product;
import com.org.onlineshoppingapplication.exception.CustomException;
import com.org.onlineshoppingapplication.exception.ProductDetailsException;
import com.org.onlineshoppingapplication.exception.ProductError;
import com.org.onlineshoppingapplication.exception.ProductException;
import com.org.onlineshoppingapplication.exception.QuantityNotAvailable;
import com.org.onlineshoppingapplication.exception.RegisterException;
import com.org.onlineshoppingapplication.exception.UserNotLoginException;

public interface ProductService {

	List<Product> products(String emailId) throws UserNotLoginException, CustomException;
	
	ResponseDto buyProduct(OrderDto orderDto) throws CustomException, RegisterException, QuantityNotAvailable;
	
	List<Product> getProductsByName(String productName) throws ProductError;
	
	ProductResponse addProductByAdmin(ProductDto productDto) throws ProductDetailsException, ProductException;

}
