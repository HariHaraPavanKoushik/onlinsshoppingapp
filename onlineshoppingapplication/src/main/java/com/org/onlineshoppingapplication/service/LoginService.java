package com.org.onlineshoppingapplication.service;

import com.org.onlineshoppingapplication.dto.LoginDto;
import com.org.onlineshoppingapplication.dto.LoginResponseDto;

/**
 * @author Uma
 *
 */
public interface LoginService {

	public LoginResponseDto userLogin(LoginDto loginDto);

}