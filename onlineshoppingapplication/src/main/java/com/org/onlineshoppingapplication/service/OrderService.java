package com.org.onlineshoppingapplication.service;

import com.org.onlineshoppingapplication.dto.OrderResponse;
import com.org.onlineshoppingapplication.dto.OrdersDto;

public interface OrderService {

	OrderResponse rating(Long productId, Long userId, OrdersDto orderDto);

}