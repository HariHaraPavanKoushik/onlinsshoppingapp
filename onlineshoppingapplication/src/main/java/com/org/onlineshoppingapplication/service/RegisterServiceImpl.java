package com.org.onlineshoppingapplication.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.org.onlineshoppingapplication.dto.RegisterResponse;
import com.org.onlineshoppingapplication.dto.UserDto;
import com.org.onlineshoppingapplication.entity.User;
import com.org.onlineshoppingapplication.repository.RegisterRepository;
import com.org.onlineshoppingapplication.utility.UserUtility;

/**
 * 
 * @author Ashan
 * @since 23-03-2020
 *
 */
@Service
public class RegisterServiceImpl implements RegisterService {

	@Autowired
	RegisterRepository registerRepository;

	/**
	 *  This functionality is used for registering the User
	 * 
	 * @param userDto
	 * @return 	-save user data and gives status code and success message
	 */
	@Override
	public RegisterResponse register(UserDto userDto) {
		

		User user = new User();

		BeanUtils.copyProperties(userDto, user);
		User userIn = registerRepository.findByEmailId(user.getEmailId());
		if (userIn !=null) {
			return new RegisterResponse(UserUtility.USER_NOT_FOUND, HttpStatus.BAD_REQUEST.value());		
		} else {
			registerRepository.save(user);
			return new RegisterResponse(UserUtility.SUCCESSSFULLY_SAVED_DATA, HttpStatus.CREATED.value());
		}
	}

}
