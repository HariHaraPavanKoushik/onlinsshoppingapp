package com.org.onlineshoppingapplication.service;

import com.org.onlineshoppingapplication.dto.RegisterResponse;
import com.org.onlineshoppingapplication.dto.UserDto;

public interface RegisterService {

	public RegisterResponse register(UserDto userDto);

}