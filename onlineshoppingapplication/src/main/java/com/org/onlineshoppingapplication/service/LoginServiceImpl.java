package com.org.onlineshoppingapplication.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.org.onlineshoppingapplication.dto.LoginDto;
import com.org.onlineshoppingapplication.dto.LoginResponseDto;
import com.org.onlineshoppingapplication.dto.ResponseDto;
import com.org.onlineshoppingapplication.entity.User;
import com.org.onlineshoppingapplication.repository.LoginRepository;
import com.org.onlineshoppingapplication.utility.UserUtility;

 

/**
 * @author Uma
 * @since 2020-03-23
 *
 */
@Service
public class LoginServiceImpl implements LoginService {
    @Autowired
    LoginRepository loginRepository;

 
    /**
     * This method is used to validate the user login
	 * 
	 
	 * @param LoginDto	-contains the request params EmailId and password ResponseDto-
	 *                          contains success or failure message as a response
	 * @return ResponseEntity Object along with status code and message
     */
    @Override
    public LoginResponseDto userLogin(LoginDto loginDto) {
    	LoginResponseDto responseDto = new LoginResponseDto();
        if (loginDto.getEmailId().isEmpty() || loginDto.getPassword().isEmpty()) {
            responseDto.setStatusCode(HttpStatus.BAD_REQUEST.value());
            responseDto.setStatusMessage("Please provide the required  data");
        } else {
            User user = loginRepository.findByEmailIdAndPassword(loginDto.getEmailId(), loginDto.getPassword());

 

            if (user != null) {
            	responseDto.setUserType(user.getUserType());
                responseDto.setUserId(user.getRegisterId());
                responseDto.setStatusCode(HttpStatus.OK.value());
                responseDto.setStatusMessage("Logged in successfully!!..");
            } else {
                responseDto.setStatusCode(UserUtility.USER_NOT_LOGIN_STATUS);
                responseDto.setStatusMessage("Please Register");
            }
        }
        return responseDto;
    }

 

}