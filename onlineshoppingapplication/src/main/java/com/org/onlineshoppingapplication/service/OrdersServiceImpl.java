package com.org.onlineshoppingapplication.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.onlineshoppingapplication.dto.OrderResponse;
import com.org.onlineshoppingapplication.dto.OrdersDto;
import com.org.onlineshoppingapplication.entity.Product;
import com.org.onlineshoppingapplication.entity.User;
import com.org.onlineshoppingapplication.entity.UserOrder;
import com.org.onlineshoppingapplication.repository.OrderRepository;
import com.org.onlineshoppingapplication.repository.ProductRepository;
import com.org.onlineshoppingapplication.repository.UserRepository;
import com.org.onlineshoppingapplication.utility.UserUtility;

/**
 * @author ashan
 * @since 2020-03-26
 *
 */
@Service
public class OrdersServiceImpl implements OrderService {

	@Autowired
	OrderRepository orderRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	ProductRepository productRepository;

	/**
	 * This method is used to add rating for the particular product and average
	 * rating
	 * 
	 * 
	 * @param OrderDto -contains the request params productRating ResponseDto-
	 *                 contains success or failure message as a response
	 * 
	 * @PathParam productId ,userId used to update the productRating
	 * @return OrderResponse Object along with status code and message
	 */

	@Override
	public OrderResponse rating(Long productId, Long userId, OrdersDto orderDto) {
		OrderResponse orderResponse = new OrderResponse();
		UserOrder orders = new UserOrder();

		BeanUtils.copyProperties(orderDto, orders);
		System.out.println(orders.getProductRating());
		Optional<User> userid = userRepository.findById(userId);
		Optional<Product> productid = productRepository.findById(productId);
//		UserOrder order = orderid.get();
//		Product product = productid.get();
		if (!userid.isPresent()) {

			orderResponse.setMessage(UserUtility.USER_NOT_FOUND);
			orderResponse.setStatusCode(UserUtility.USER_NOT_FOUND_STATUS);

		} else {
			if (!productid.isPresent()) {
				orderResponse.setMessage(UserUtility.NO_PRODUCT_FOUND);
				orderResponse.setStatusCode(UserUtility.NO_PRODUCT_PRESENT_ERRORCODE);
			} else {

//			orderRepository.updateRating(productId, userId, orders.getProductRating());
				UserOrder orderDetail = orderRepository.findByUserRegisterIdAndProductProductId(userId, productId);
				if (orderDetail != null ) {
					System.out.println(orders.getProductRating());
					orderDetail.setProductRating(orders.getProductRating());
					if (productid.get().getProductRating() != 0) {

						int orderRating = orders.getProductRating();

						double productRating1 = productid.get().getProductRating();

						double avg = (orderRating + productRating1) / 2;

						productid.get().setProductRating(avg);
					} else {
						productid.get().setProductRating(orderDetail.getProductRating());
					}
					productRepository.save(productid.get());

					orderResponse.setMessage(UserUtility.RATTED_SUCCESSFULLY);
					orderResponse.setStatusCode(UserUtility.RATTED_SUCCESSFUL_CODE);
				} else {
					orderResponse.setMessage(UserUtility.ORDER_NOT_EXIST);
					orderResponse.setStatusCode(UserUtility.ORDER_NOT_EXIST_STATUS);
				}

			}

		}
		return orderResponse;
	}
}