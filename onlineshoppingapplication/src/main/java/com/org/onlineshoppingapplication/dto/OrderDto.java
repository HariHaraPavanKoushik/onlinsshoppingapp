package com.org.onlineshoppingapplication.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OrderDto {

	int quantity;

	long userId;

	long productId;
}
