package com.org.onlineshoppingapplication.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductResponse {
	String message;
	int statusCode;

	public ProductResponse(String message, int statusCode) {
		super();
		this.message = message;
		this.statusCode = statusCode;
	}

}