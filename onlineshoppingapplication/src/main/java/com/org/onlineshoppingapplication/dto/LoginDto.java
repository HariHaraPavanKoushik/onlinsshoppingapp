package com.org.onlineshoppingapplication.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Uma
 *
 */
@Setter
@Getter
public class LoginDto {

	private String emailId;
	private String password;

}
