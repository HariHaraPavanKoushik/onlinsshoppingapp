package com.org.onlineshoppingapplication.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginResponseDto {

	private String userType;
	private long userId;
	private int statusCode;
	private String statusMessage;
}
