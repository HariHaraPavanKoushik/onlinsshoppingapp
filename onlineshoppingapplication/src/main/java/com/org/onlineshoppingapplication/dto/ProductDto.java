package com.org.onlineshoppingapplication.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductDto {

	String productName;
	double productCost;
	String availability;
	String productType;
	private int quantity;
	private double productRating;
}
