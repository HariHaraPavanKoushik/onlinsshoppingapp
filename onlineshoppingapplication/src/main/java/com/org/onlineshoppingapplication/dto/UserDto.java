package com.org.onlineshoppingapplication.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserDto {

	String userName;
	String password;
	String emailId;
	long mobileNumber;
	String address;
	String userType;
}
