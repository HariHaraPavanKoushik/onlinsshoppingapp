package com.org.onlineshoppingapplication.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RegisterResponse {
    
    String message;
     int statusCode;
  
    
    public RegisterResponse(String message, int statusCode) {
        super();
        this.message = message;
        this.statusCode = statusCode;
    }

 


}
