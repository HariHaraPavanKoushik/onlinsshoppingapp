package com.org.onlineshoppingapplication.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "REGISTER_ID")
	long registerId;
	String userName;
	String password;
	
	@Column(unique = true)
	String emailId;
	long mobileNumber;
	String address;
	String userType;
	
	
	
	  @JsonIgnore
	  
	  @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
	  
	  private List<UserOrder> userOrder ;
	 
	 
}
