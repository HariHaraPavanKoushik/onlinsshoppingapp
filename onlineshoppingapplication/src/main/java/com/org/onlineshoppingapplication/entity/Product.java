package com.org.onlineshoppingapplication.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class Product implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PRODUCT_ID")
	private long productId;
	private String productName;
	private double productCost;
	private String availability;
	private String productType;
	private int quantity;
	private double productRating;
	
	
	
	  @JsonIgnore
	  @OneToMany(cascade = CascadeType.ALL, mappedBy = "product")
	  
	  private List<UserOrder> userOrder ;
	 
	 
	
}
