package com.org.onlineshoppingapplication.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class UserOrder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long orderId;
	
	private int quantity;
	private int productRating;
	private double totalcost;
	
	@ManyToOne
	private User user;
	
	@ManyToOne
	private Product product;
	
}
