package com.org.onlineshoppingapplication.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.org.onlineshoppingapplication.entity.Product;
import com.org.onlineshoppingapplication.entity.User;
import com.org.onlineshoppingapplication.exception.CustomException;
import com.org.onlineshoppingapplication.exception.ProductError;
import com.org.onlineshoppingapplication.exception.UserNotLoginException;
import com.org.onlineshoppingapplication.repository.ProductRepository;
import com.org.onlineshoppingapplication.repository.UserRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ProductServiceImplTest {
	@InjectMocks
	ProductServiceImpl productServiceImpl;
	@Mock
	UserRepository userRepository;
	@Mock
	ProductRepository productRepository;

	@Test
	public void productsTest() throws CustomException, UserNotLoginException {
		User user = new User();
		user.setEmailId("pavan@email.com");
		List<Product> product1 = new ArrayList<Product>();

		Product product = new Product();
		product.setProductName("mobile");
		product.setProductCost(23456.99);
		product1.add(product);
		Mockito.when(userRepository.getProductType(user.getEmailId())).thenReturn("high");
		Mockito.when(productRepository.getProductDetails("high")).thenReturn(product1);
		productServiceImpl.products("pavan@email.com");
		assertEquals(1, product1.size());

	}

	@Test
	public void productsTest2() throws CustomException, UserNotLoginException {
		User user = new User();
		user.setEmailId("pavan@email.com");
		List<Product> product1 = new ArrayList<Product>();

		Product product = new Product();
		product.setProductName("mobile");
		product.setProductCost(23456.99);
		product1.add(product);
		Mockito.when(userRepository.getProductType(user.getEmailId())).thenReturn("low");
		Mockito.when(productRepository.getProductDetails("low")).thenReturn(product1);
		productServiceImpl.products("pavan@email.com");
		assertEquals(1, product1.size());

	}

	@Test(expected = CustomException.class)
	public void productsTestNegative() throws CustomException, UserNotLoginException {
		User user = new User();
		user.setEmailId("pavan@email.com");

		Product product = new Product();
		product.setProductName("mobile");
		product.setProductCost(23456.99);

		Mockito.when(userRepository.getProductType(user.getEmailId())).thenReturn(null);

		productServiceImpl.products("pavan@email.com");

	}

	@Test
	public void getProductByName() throws ProductError {
		List<Product> list = new ArrayList<>();
		Product product = new Product();
		product.setProductName("pixel");
		product.setProductCost(23456.99);
		list.add(product);
		Mockito.when(productRepository.fetchProductByName(Mockito.anyString())).thenReturn(list);
		productServiceImpl.getProductsByName("pixel");

	}

	@Test(expected = ProductError.class)
	public void getProductByNameNegative() throws ProductError {

		Mockito.when(productRepository.fetchProductByName(Mockito.anyString())).thenReturn(null);
		productServiceImpl.getProductsByName("pixel");

	}
}