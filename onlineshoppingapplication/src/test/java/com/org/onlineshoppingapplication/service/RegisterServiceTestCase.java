package com.org.onlineshoppingapplication.service;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import com.org.onlineshoppingapplication.dto.RegisterResponse;
import com.org.onlineshoppingapplication.dto.UserDto;
import com.org.onlineshoppingapplication.entity.User;
import com.org.onlineshoppingapplication.exception.RegisterException;
import com.org.onlineshoppingapplication.repository.RegisterRepository;
import com.org.onlineshoppingapplication.utility.UserUtility;

@RunWith(MockitoJUnitRunner.Silent.class)
public class RegisterServiceTestCase {

	@Mock
	RegisterRepository registerRepository;

	@InjectMocks
	RegisterServiceImpl registerServiceImpl;

	RegisterResponse response = new RegisterResponse("Account registered Successfully", HttpStatus.CREATED.value());

	RegisterResponse response2 = new RegisterResponse("Please register first!", HttpStatus.BAD_REQUEST.value());

	User userDto = new User();

	@Before
	public void setup() {
		userDto.setUserName("John");
		userDto.setEmailId("john@gmail.com");
		userDto.setAddress("mettur");
		userDto.setPassword("abc");
		userDto.setMobileNumber(9876543210l);
		userDto.setRegisterId(1);
		userDto.setUserType("p");

	}

	@Test
	public void testRegister() throws RegisterException {
		UserDto accountRegistrationDto = new UserDto();

		Mockito.when(registerRepository.save(userDto)).thenReturn(userDto);
		RegisterResponse registerResponse = registerServiceImpl.register(accountRegistrationDto);
		assertEquals(HttpStatus.CREATED.value(), registerResponse.getStatusCode());
	}

	@Test
	public void registerTest2() {

		UserDto userDto = new UserDto();
		userDto.setEmailId("uma@hcl.com");
		userDto.setUserName("uma");
		RegisterResponse registerResponse = new RegisterResponse(UserUtility.USER_NOT_FOUND,
				HttpStatus.BAD_REQUEST.value());
		registerResponse.setMessage("please Register");
		registerResponse.setStatusCode(HttpStatus.BAD_REQUEST.value());
		User userIn = new User();
		userIn.setEmailId("uma@hcl.com");
		userIn.setPassword("uma");
		Mockito.when(registerRepository.findByEmailId("uma@hcl.com")).thenReturn(userIn);
		RegisterResponse result = registerServiceImpl.register(userDto);
		assertEquals(registerResponse.getStatusCode(), result.getStatusCode());

	}

}
