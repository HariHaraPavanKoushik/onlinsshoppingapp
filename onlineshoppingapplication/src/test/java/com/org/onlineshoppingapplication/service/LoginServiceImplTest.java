package com.org.onlineshoppingapplication.service;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import com.org.onlineshoppingapplication.dto.LoginDto;
import com.org.onlineshoppingapplication.dto.LoginResponseDto;
import com.org.onlineshoppingapplication.entity.User;
import com.org.onlineshoppingapplication.repository.LoginRepository;

@RunWith(MockitoJUnitRunner.Silent.class)
public class LoginServiceImplTest {

	@InjectMocks
	LoginServiceImpl loginServiceImpl;

	@Mock
	LoginRepository loginRepository;
	
	@Before
	public void setup() {
		
	}

	@Test
	public void userLoginTest() {
		LoginDto loginDto = new LoginDto();
		loginDto.setEmailId("uma@hcl.com");
		loginDto.setPassword("uma@123");
		LoginResponseDto responseDto = new LoginResponseDto();
		responseDto.setStatusCode(HttpStatus.OK.value());
		responseDto.setStatusMessage("Logged in successfully!!..");

		User user = new User();
		user.setEmailId("uma@hcl.com");
		user.setPassword("uma@123");
		user.setMobileNumber(99887766);
		Mockito.when(loginRepository.findByEmailIdAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(user);
		LoginResponseDto res = loginServiceImpl.userLogin(loginDto);
		assertEquals(responseDto.getStatusCode(), res.getStatusCode());

	}

	@Test
	public void negativeUserLoginTest() {
		LoginDto loginDto = new LoginDto();
		loginDto.setEmailId("");
		loginDto.setPassword("");
		LoginResponseDto responseDto = new LoginResponseDto();
		responseDto.setStatusCode(HttpStatus.BAD_REQUEST.value());
		responseDto.setStatusMessage("Please provide the required  data");

		User user = new User();
		user.setEmailId("uma@hcl.com");
		user.setPassword("uma123");
		user.setMobileNumber(998877661);
		Mockito.when(loginRepository.findByEmailIdAndPassword(loginDto.getEmailId(),loginDto.getPassword()))
				.thenReturn(user);
		LoginResponseDto res = loginServiceImpl.userLogin(loginDto);
		assertEquals(responseDto.getStatusCode(), res.getStatusCode());

	}

	@Test
	public void NoUserLoginTest() {
		LoginDto loginDto = new LoginDto();
		loginDto.setEmailId("uma@hcl.com");
		loginDto.setPassword("uma123");
		LoginResponseDto responseDto = new LoginResponseDto();
		responseDto.setStatusCode(605);
		responseDto.setStatusMessage("Please Register");

		User user = new User();
		user.setEmailId("uma@hcl.com");
		user.setPassword("uma123");
		user.setMobileNumber(998877662);
		Mockito.when(loginRepository.findByEmailIdAndPassword("pal", "pal")).thenReturn(user);
		LoginResponseDto res = loginServiceImpl.userLogin(loginDto);
		assertEquals(responseDto.getStatusCode(), res.getStatusCode());

	}

}
