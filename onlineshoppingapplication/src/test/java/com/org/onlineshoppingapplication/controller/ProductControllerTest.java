package com.org.onlineshoppingapplication.controller;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.org.onlineshoppingapplication.dto.ProductDto;
import com.org.onlineshoppingapplication.dto.ProductResponse;
import com.org.onlineshoppingapplication.entity.Product;
import com.org.onlineshoppingapplication.exception.CustomException;
import com.org.onlineshoppingapplication.exception.ProductDetailsException;
import com.org.onlineshoppingapplication.exception.ProductException;
import com.org.onlineshoppingapplication.exception.UserNotLoginException;
import com.org.onlineshoppingapplication.service.ProductService;
import com.org.onlineshoppingapplication.utility.UserUtility;

@RunWith(MockitoJUnitRunner.class)
public class ProductControllerTest {
	@InjectMocks
	ProductController productController;
	@Mock
	ProductService productService;

	@Test
	public void productsTest() throws CustomException, UserNotLoginException {
		List<Product> product1 = new ArrayList<Product>();

		Product product = new Product();
		product.setProductName("mobile");
		product.setProductCost(23456.99);
		Mockito.when(productService.products("uma@gmail.com")).thenReturn(product1);
		ResponseEntity<List<Product>> result = productController.products("uma@gmail.com");
		assertEquals(HttpStatus.OK, result.getStatusCode());

	}

	@Test
	public void getProductListByName() throws CustomException {
		List<Product> product1 = new ArrayList<Product>();

		Product product = new Product();
		product.setProductName("pixel");
		product.setProductCost(23456.99);
		Mockito.when(productService.getProductsByName(product.getProductName())).thenReturn(product1);
		ResponseEntity<List<Product>> result = productController.productsbyName("pixel");
		assertEquals(HttpStatus.OK, result.getStatusCode());
	}
	
	@Test
    public void addProductTest() throws ProductDetailsException, ProductException {
        ProductDto productDto = new ProductDto();
        productDto.setProductName("oneplus");
        productDto.setProductCost(25678.33);
        productDto.setProductType("priority");
        productDto.setAvailability("in stock");
        ProductResponse productResponse = new ProductResponse(UserUtility.SUCCESSSFULLY_SAVED_PRODUCT_DATA,
                HttpStatus.CREATED.value());
        productResponse.setMessage("Register successfully");
        productResponse.setStatusCode(HttpStatus.OK.value());
        Mockito.when(productService.addProductByAdmin(productDto)).thenReturn(productResponse);
        ResponseEntity<ProductResponse> result = productController.addProductByAdmin(productDto);
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

}
