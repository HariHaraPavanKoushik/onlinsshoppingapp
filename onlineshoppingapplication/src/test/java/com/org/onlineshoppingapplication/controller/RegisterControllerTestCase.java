package com.org.onlineshoppingapplication.controller;



import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.org.onlineshoppingapplication.dto.RegisterResponse;
import com.org.onlineshoppingapplication.dto.UserDto;
import com.org.onlineshoppingapplication.exception.RegisterException;
import com.org.onlineshoppingapplication.service.RegisterService;
import com.org.onlineshoppingapplication.utility.UserUtility;

 

@RunWith(MockitoJUnitRunner.class)
public class RegisterControllerTestCase {
    @InjectMocks
    RegisterController registerController;
    @Mock
    RegisterService registerService;

 

    @Test
    public void registerTest() throws RegisterException {
        UserDto userDto = new UserDto();
        userDto.setEmailId("uma@gmail.com");
        userDto.setPassword("uma123");
        RegisterResponse registerResponse = new RegisterResponse(UserUtility.SUCCESSSFULLY_SAVED_DATA,
                HttpStatus.CREATED.value());
        registerResponse.setMessage("Register successfully");
        registerResponse.setStatusCode(HttpStatus.OK.value());
        Mockito.when(registerService.register(userDto)).thenReturn(registerResponse);
        ResponseEntity<RegisterResponse> result = registerController.register(userDto);
        assertEquals(HttpStatus.CREATED, result.getStatusCode());

 

    }
}