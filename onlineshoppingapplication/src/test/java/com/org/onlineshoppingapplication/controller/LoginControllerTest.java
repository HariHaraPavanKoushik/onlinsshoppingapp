package com.org.onlineshoppingapplication.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.org.onlineshoppingapplication.dto.LoginDto;
import com.org.onlineshoppingapplication.dto.LoginResponseDto;
import com.org.onlineshoppingapplication.service.LoginService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class LoginControllerTest {
	@InjectMocks
	LoginController loginController;
	@Mock
	LoginService loginService;

	@Test
	public void userLoginTest() {
		LoginDto loginDto = new LoginDto();
		loginDto.setPassword("uma123");
		loginDto.setEmailId("uma@hcl.com");
		LoginResponseDto responseDto = new LoginResponseDto();

		responseDto.setStatusCode(HttpStatus.BAD_REQUEST.value());
		responseDto.setStatusMessage("Please provide the required  data");

		Mockito.when(loginService.userLogin(loginDto)).thenReturn(responseDto);
		ResponseEntity<LoginResponseDto> result = loginController.userLogin(loginDto);

		assertEquals(HttpStatus.OK, result.getStatusCode());

	}

}
